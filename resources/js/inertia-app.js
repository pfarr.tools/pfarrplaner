/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {InertiaApp} from '@inertiajs/inertia-vue'
import EventBus from './plugins/EventBus.js';
import Vue from 'vue'
import {InertiaProgress} from '@inertiajs/progress'

import AdminLayout from "./Pages/Layouts/AdminLayout";

import LaravelPermission from "./plugins/LaravelPermission";
import CalendarNavTop from './components/Calendar/Nav/Top.vue';
import CalendarNavControlSidebar from './components/Calendar/Nav/ControlSidebar';
import CalendarDayHeader from './components/Calendar/Day/Header';
import CalendarCell from './components/Calendar/Cell';
import CalendarService from './components/Calendar/Service.vue';
import CalendarServiceParticipants from './components/Calendar/Service/Participants.vue';
import CalendarServiceWedding from './components/Calendar/Service/Wedding.vue';
import CalendarServiceFuneral from './components/Calendar/Service/Funeral.vue';
import CalendarServiceBaptism from './components/Calendar/Service/Baptism.vue';
import CalendarControlCitySort from './components/Calendar/Control/CitySort';

import datePicker from 'vue-bootstrap-datetimepicker';
//import 'bootswatch/dist/pulse/bootstrap.min.css'
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
import "@mdi/font/css/materialdesignicons.min.css"


window._ = require('lodash');


/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
try {
    window.Popper = require('@popperjs/core');
    require('bootstrap/dist/js/bootstrap.bundle.min');
} catch (e) {}

window.$ = $.noConflict();
window.moment = require('moment');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.api = require('axios');
window.api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.api.defaults.withCredentials = true;


/**
* Next we will register the CSRF Token as a common header with Axios so that
* all outgoing HTTP requests automatically have it attached. This is just
* a simple convenience so we don't have to attach every token manually.
*/

window.token = async function() {
    console.log('Checking CSRF token...');
    currentToken = document.head.querySelector('meta[name="csrf-token"]');

    if (currentToken) {
        console.log('CSRF token is present.');
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = currentToken.content;
        window.api.defaults.headers.common['X-CSRF-TOKEN'] = currentToken.content;
        return currentToken.content;
    } else {
        try {
            console.log('Retrieving new CSRF token...');
            const response = await window.axios.get(route('csrf.keepalive'));
            currentToken = response.data.token;
            window.axios.defaults.headers.common['X-CSRF-TOKEN'] = currentToken;
            window.api.defaults.headers.common['X-CSRF-TOKEN'] = currentToken;
            document.querySelector('meta[name="description"]').setAttribute("content", currentToken);
            console.log('CSRF token is present.');
            return currentToken;
        } catch(error) {
            console.error('Error retrieving token: '+error);
            return null;
        }
    }

}
window.csrf_token = window.token();




/**
 * Bootstrap plugins etc.
 */

//require('admin-lte');
window.moment = require('moment');




/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */
let currentToken = document.head.querySelector('meta[name="csrf-token"]');
if (currentToken) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = currentToken.content;
    window.api.defaults.headers.common['X-CSRF-TOKEN'] = currentToken.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import 'admin-lte/dist/js/adminlte.min';



/**
 * VUE app configuration
 */


Vue.use(datePicker);
Vue.component('admin-layout', AdminLayout)
InertiaProgress.init({
    delay: 100,
    color: '#29d',
    includeCSS: true,
    showSpinner: true,
});
Vue.use(InertiaProgress);
Vue.use(LaravelPermission);
Vue.use(EventBus);
Vue.config.productionTip = false

Vue.mixin({
    methods: {
        route: route,
        moment: moment
    }
});
Vue.mixin(require('./mixins/Asset.js'));
Vue.mixin(require('./mixins/PfarrplanerAPI'));


// Register a global custom directive called `v-focus`
Vue.directive('focus', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        // Focus the element
        el.focus()
        el.select();
    }
})

// Register a global custom directive called `v-scrollTo`
Vue.directive('scrollTo', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        // Focus the element
        el.scrollTo()
    }
})

const bindCustomEvent = {
    getName: function(binding) {
        return binding.arg + '.' +
            Object.keys(binding.modifiers).map(key => key).join('.');
    },
    bind: function(el, binding, vnode) {
        const eventName = bindCustomEvent.getName(binding);
        document.addEventListener(eventName, binding.value);
    },
    unbind: function(el, binding) {
        const eventName = bindCustomEvent.getName(binding);
        document.removeEventListener(eventName, binding.value);
    }
};

// register a global custom directive called v-bind-custom-event
Vue.directive('bindCustomEvent', bindCustomEvent);



Vue.use(InertiaApp)

jQuery.extend(true, jQuery.fn.datetimepicker.defaults, {
    icons: {
        time: 'mdi mdi-clock',
        date: 'mdi mdi-calendar',
        up: 'mdi mdi-arrow-up',
        down: 'mdi mdi-arrow-down',
        previous: 'mdi mdi-chevron-left',
        next: 'mdi mdi-chevron-right',
        today: 'mdi mdi-calendar-check',
        clear: 'mdi mdi-delete',
        close: 'mdi mdi-close'
    }
});


const app = document.getElementById('app')

window.vm = new Vue({
    render: h => h(InertiaApp, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
        },
    }),
}).$mount(app)

