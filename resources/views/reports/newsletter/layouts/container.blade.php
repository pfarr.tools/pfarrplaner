<table border="0" cellpadding="0" cellspacing="0" width="100%"
       style="background-color: #F1F5F7; ;;;width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; "
       class="bgcolor-1 editable" data-style="background-color:inherit" data-limit-to="background,background-color"
       data-name="Outer Container">
    <tbody>
    <tr>
        <td style="font-size: 0px; ">
            <!--[if mso | IE]>
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style=""
                   width="600" gc-width-fix=".cr-maxwidth:max-width">
                <tr>
                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
            <div class="cr-maxwidth" style="max-width: 600px; ;;;margin:0px auto;">
                <table align="center" border="0" cellpadding="0" cellspacing="0"
                       style="max-width: 600px; ;;;background-color: #FFFFFF; ;;;width:100%; "
                       class="cr-maxwidth bgcolor-2 editable" data-style="background-color:inherit"
                       data-limit-to="background,background-color" data-name="Container">
                    <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;text-align:center;">
                            <!--[if mso | IE]>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td><![endif]-->

                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                <tr>
                                    <td class="cr-text color-2"
                                        style="color: #082137; ;;;font-family: Helvetica, Arial, sans-serif; font-size: 14px; ;;;padding:10px 20px;"
                                        data-style="padding:inherit;color:inherit;" data-name="Text">
                                        <!--#html#-->
                                        @yield('content')
                                        <!--#/html#-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <!--[if mso | IE]></td></tr></table><![endif]-->

        </td>
    </tr>
    </tbody>
</table>
