<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Api;

use App\Models\Liturgy\Songbook;
use Illuminate\Http\Request;

class SongbookController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get a list of all songbooks
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Songbook::orderBy('code')->get());
    }

    /**
     * Store a new songbook
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) {
        $data = $this->validateRequest($request);
        $songbook = Songbook::create($data);
        return response()->json($songbook);
    }

    /**
     * Get default colors for songbooks
     * @return \Illuminate\Http\JsonResponse
     */
    public function colors()
    {
        $data = [['name' => 'Ohne Abschnittsfarbe', 'id' => '']];
        foreach (config('colors.songbooks', []) as $name => $color) {
            $data[] = ['name' => $name, 'id' => $color];
        }
        return response()->json($data);
    }

    /**
     * Validate submitted data
     *
     * @param Request $request
     * @return array
     */
    protected function validateRequest(Request $request) {
        return $request->validate([
            'name' => 'required|string',
            'code' => 'required|string',
            'isbn' => 'nullable|string',
            'description' => 'nullable|string',
            'image' => 'nullable|string',
                                  ]);
    }
}
