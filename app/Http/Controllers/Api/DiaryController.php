<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Api;

use App\Calendars\Exchange\ExchangeCalendar;
use App\Models\Calendar\External\CalendarConnection;
use App\Models\Calendar\External\CalendarConnectionEntry;
use App\Models\DiaryEntry;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiaryController extends \App\Http\Controllers\Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Add a record from an existing sermon
     *
     * @param $category Target category
     * @param Service $service Service record
     * @return JsonResponse New DiaryEntry record
     */
    public function addService($category, Service $service)
    {
        return response()->json(DiaryEntry::createFromService($service, $category));
    }

    /**
     * Add a record from a calendar event
     *
     * @param $category Target category
     * @return JsonResponse New DiaryEntry record
     */
    public function addEvent(Request $request, $category)
    {
        $eventData = $request->get('event', null);
        if (!$eventData) {
            abort(500);
        }
        return response()->json(DiaryEntry::createFromEvent($eventData, $category));
    }

    /**
     * Move DiaryEntry to another category
     *
     * @param $category Target category
     * @param DiaryEntry $diaryEntry Diary entry
     * @return JsonResponse Diary entry with updated info
     */
    public function moveItem($category, DiaryEntry $diaryEntry)
    {
        $diaryEntry->update(['category' => $category]);
        return response()->json($diaryEntry);
    }

    /**
     * Store a new DiaryEntry in the database
     *
     * @param Request $request
     * @return JsonResponse Diary entry
     */
    public function store(Request $request)
    {
        $data =             $request->validate([
                                                   'title' => 'required|string',
                                                   'date' => 'required|date',
                                                   'category' => 'required|string',
                                               ]);
        $data['user_id'] = Auth::user()->id;
        $data['date'] = Carbon::parse($data['date'], 'UTC')->setTimezone('Europe/Berlin');
        $diaryEntry = DiaryEntry::create($data);
        return response()->json($diaryEntry);
    }

    public function update(Request $request, DiaryEntry $diaryEntry)
    {
        $diaryEntry->update(
            $request->validate([
                                   'title' => 'required|string',
                                   'date' => 'required|date',
                                   'category' => 'required|string',
                               ])
        );
        return response()->json($diaryEntry);
    }

    /**
     * Delete a record
     *
     * @param DiaryEntry $diaryEntry DiaryEntry to be deleted
     * @return JsonResponse Service record for the deleted item
     */
    public function destroy(DiaryEntry $diaryEntry)
    {
        $service = $diaryEntry->service;
        if (!$service) {
            $service = ['date' => $diaryEntry->date, 'title' => $diaryEntry->title];
        }
        $diaryEntry->delete();
        return response()->json($service);
    }


    public function getCalendar(CalendarConnection $calendarConnection, $date)
    {
        $start = Carbon::parse($date . '-01 0:00:00');
        $end = $start->copy()->endOfMonth();

        // get UIDs for event already contained in diary, i.e. to be filtered out
        $uidsWithExistingEntry = DiaryEntry::select('event_id')
            ->whereDate('date', '>=', $start)
            ->whereDate('date', '<=', $end)
            ->where('user_id', Auth::user()->id)
            ->get()
            ->pluck('event_id');

        // get calendar ids for services to be filtered out
        $uidsWithServiceRecord = collect();
        $servicesInMonth = Service::with([])->select('id')->between($start, $end)->get()->pluck('id');
        foreach ($servicesInMonth as $serviceId) {
            $entry = CalendarConnectionEntry::where('calendar_connection_id', $calendarConnection->id)
                ->where('service_id', $serviceId)
                ->first();
            if ($entry) $uidsWithServiceRecord->push($entry->foreign_id);
        }
        $uidsWithServiceRecord->push("040000008200E00074C5B7101A82E0080000000076510F975C3FD801000000000000000010000000A14DEB7A1BEA794B9B187FDDB36E92FF");


        /** @var ExchangeCalendar $calendar */
        $calendar = $calendarConnection->getSyncEngine()->getCalendar();
        $events = $calendar->getAllEventsForRange($start, $end);

        $days = [];
        foreach ($events as $event) {
            // filter out existing entries and service entries
            if ((!$uidsWithExistingEntry->contains($event->UID)) && (!$uidsWithServiceRecord->contains($event->UID))) {
                if ($event->TimeZone == 'UTC') {
                    $tzCode = 'UTC';
                } else {
                    preg_match('/[\+\-]\d\d\:\d\d/', $event->TimeZone, $matches);
                    $tzCode = $matches[0];
                }

                $days[Carbon::parse($event->Start, $tzCode)->setTimezone('Europe/Berlin')->format('Y-m-d')][] = $event;
            }
        }

        return response()->json($days);
    }

}
