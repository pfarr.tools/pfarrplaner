<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Models\Calendar\Day;
use App\Models\LiturgyInfo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Storage;

/**
 * Class Liturgy
 * @package App
 */
class LiturgyService
{

    /** @var Liturgy|null Instance */
    protected static $instance = null;

    protected static $bibleReferences = [
        'litTextsWeeklyPsalm',
        'litTextsWeeklyQuote',
        'litTextsEntryPsalm',
        'litTextsOldTestament',
        'litTextsEpistel',
        'litTextsEvangelium',
        'litTextsPreacher',
        'litTextsHaleluja',
        'litTextsPerikope1',
        'litTextsPerikope2',
        'litTextsPerikope3',
        'litTextsPerikope4',
        'litTextsPerikope5',
        'litTextsPerikope6',
        'litTextsSpecialty',
        'currentPerikope',
    ];

    /**
     * @return Liturgy|null
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function getCompleteLiturgyInfoArray(): array
    {
        if (!Storage::exists('liturgy.json')) {
            return [];
        }
        $tmpData = json_decode(Storage::get('liturgy.json'), true);
        $overrides = config('liturgy')['overrides'];
        foreach ($tmpData['content']['days'] as $key => $val) {
            $dateComponents = explode('.', $val['date']);
            if (isset($overrides[$dateComponents[1]][$dateComponents[0]])) {
                $val = array_replace_recursive($val, $overrides[$dateComponents[1]][$dateComponents[0]]);
            }
            if (!isset($data[$val['date']])) {
                $data[$val['date']] = $val;
            }
        }
        return $data ?: [];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getCompleteLiturgyInfoCollection(): Collection
    {
        if (!Storage::exists('liturgy.json')) {
            return collect();
        }
        $list = json_decode(Storage::get('liturgy.json'), true)['content']['days'];
        $overrides = config('liturgy.overrides');
        foreach ($list as $key => $val) {
            $list[$key]['title'] = $overrides[$val['title']] ?? $val['title'];
        }
        return collect($list);
    }

    public static function getLiturgyInfoByDate($date)
    {
        if (!is_a(Carbon::class, $date)) {
            $date = Carbon::parse($date);
        }
        return LiturgyInfo::whereDate('date', $date->format('Y-m-d'))->get();
    }

    public static function getLiturgyInfoByDayId($dayId = null)
    {
        $list = self::getCompleteLiturgyInfoCollection()->groupBy('dayId');
        if ($dayId) return $list[$dayId] ?? null;
        return $list;
    }

    /**
     * Get all the liturgical info for a given day
     * @param string|Carbon $day
     * @param bool $fallback
     * @return array
     */
    public static function getDayInfo($date, $fallback = false): array
    {
        // fallback for obsolete code that still uses Day objects
        if (is_a($date, Day::class)) {
            $date = $date->date;
        }

        if (is_string($date)) {
            $date = Carbon::parse($date);
        }
        if (!$date) {
            return [];
        }
        $data = self::getCompleteLiturgyInfoArray();

        $result = null;
        if (isset($data[$date->format('d.m.Y')])) {
            $result = $data[$date->format('d.m.Y')];
        } elseif ($fallback) {
            $date = $date;
            while (!isset($data[$date->format('d.m.Y')])) {
                $date = $date->subDays(1);
            }
            $result = isset($data[$date->format('d.m.Y')]) ? $data[$date->format('d.m.Y')] : [];
        }
        if (!is_null($result)) {
            $result['currentPerikope'] = $result['litTextsPerikope' . $result['perikope']];
            $result['currentPerikopeLink'] = $result['litTextsPerikope' . $result['perikope'] . 'Link'];
            return $result;
        }
        return [];
    }


}
