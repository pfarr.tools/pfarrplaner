<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Models\People\User;

class NameService
{

    public const LAST_COMMA_FIRST = 1;
    public const FIRST_LAST = 2;
    public const LAST_FIRST = 3;
    public const LAST_FIRST_ARRAY = 4;
    public const TITLE_FIRST_LAST = 5;

    protected $firstName = '';
    protected $lastName = '';
    protected $title = '';

    public function __construct($firstName, $lastName, $title = '')
    {
        $this->firstName = trim($firstName);
        $this->lastName = trim($lastName);
        $this->title = trim($title);
    }

    public static function fromName($name, $title = ''): NameService {
        if (str_contains($name, ',')) {
            list($lastName, $firstName) = explode(',', $name);
        } else {
            $parts = explode(' ', $name);
            $lastName = array_pop($parts);
            $firstName = join(' ', $parts);
        }
        return new self($firstName, $lastName, $title);
    }

    public static function fromUser(User $user): NameService {
        return self::fromName($user->name, $user->title);
    }

    public function format($format = self::LAST_COMMA_FIRST)
    {
        switch($format) {
            case self::LAST_COMMA_FIRST:
                return $this->lastName.', '.$this->firstName;
            case self::FIRST_LAST:
                return $this->firstName.' '.$this->lastName;
            case self::LAST_FIRST:
                return strtoupper($this->lastName).' '.$this->firstName;
            case self::LAST_FIRST_ARRAY:
                return [$this->lastName, $this->firstName];
            case self::TITLE_FIRST_LAST:
                return trim($this->title.' '.$this->firstName.' '.$this->lastName);
        }
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getTitle(): string
    {
        return $this->title;
    }



}
