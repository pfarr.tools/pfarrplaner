<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Calendars\LocalEventCalendars;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

abstract class AbstractLocalEventCalendar
{

    protected static $group = '';

    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    abstract public static function list(): array;

    abstract public function adjustQuery(Builder $query): Builder;

    public static function getKey()
    {
        return Str::lcfirst(Str::replace(['App\\Calendars\\LocalEventCalendars\\', 'LocalEventCalendar'], ['', ''], get_called_class()));
    }

    public static function getEntry($id, $name)
    {
        return ['id' => static::getKey().':'.$id, 'name' => $name, 'group' => static::$group];
    }

    /**
     * Preset the data from the chosen local calendar for a new event
     * @param array $data
     * @return array
     */
    public function presetData(array $data): array {
        $data['event_class'] = 'event';
        return $data;
    }

}
