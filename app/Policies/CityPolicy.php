<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace App\Policies;

use App\Models\People\User;
use App\Models\Places\City;
use App\Services\RoleService;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class CityPolicy
 * @package App\Policies
 */
class CityPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        if ($user->isAdmin || $user->isLocalAdmin) {
            return true;
        }
        if ($user->can('ort-bearbeiten')) return true;
        return false;
    }

    /**
     * Determine whether the user can view the city.
     *
     * @param User $user
     * @param City $city
     * @return mixed
     */
    public function view(User $user, City $city)
    {
        return true;
    }

    /**
     * Determine whether the user can create cities.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin;
    }

    /**
     * Determine whether the user can update the city.
     *
     * @param User $user
     * @param City $city
     * @return mixed
     */
    public function update(User $user, City $city)
    {
        if (($user->can('gd-opfer-bearbeiten') || $user->can('ort-bearbeiten')) && $user->writableCities->contains(
                $city
            )) {
            return true;
        }
        if ($user->isAdmin && $user->adminCities->contains($city)) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the city.
     *
     * @param User $user
     * @param City $city
     * @return mixed
     */
    public function delete(User $user, City $city)
    {
        return $user->hasRole(RoleService::ROLE_SUPER_ADMIN);
    }

    /**
     * Determine whether the user can restore the city.
     *
     * @param User $user
     * @param City $city
     * @return mixed
     */
    public function restore(User $user, City $city)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the city.
     *
     * @param User $user
     * @param City $city
     * @return mixed
     */
    public function forceDelete(User $user, City $city)
    {
        return false;
    }
}
