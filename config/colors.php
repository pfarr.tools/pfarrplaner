<?php
/*
 * Pfarrplaner
 *
 * @package Pfarrplaner
 * @author Christoph Fischer <chris@toph.de>
 * @copyright (c) Christoph Fischer, https://christoph-fischer.org
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL 3.0 or later
 * @link https://codeberg.org/pfarr.tools/pfarrplaner
 * @version git: $Id$
 *
 * Sponsored by: Evangelischer Kirchenbezirk Balingen, https://www.kirchenbezirk-balingen.de
 *
 * Pfarrplaner is based on the Laravel framework (https://laravel.com).
 * This file may contain code created by Laravel's scaffolding functions.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

return [
    'songbooks' => [
        'EG gelb' => '#fdf538',
    ],
    'elkw' => [
        'violett' => '#8d197c',
        'dunkelviolett' => '#591855',
        'dunkelgrau' => '#474f60',
        'dunkelblau' => '#29447b',
        'taubenblau' => '#707aa0',
        'hellblau' => '#5eb3d2',
        'graublau' => '#6290b1',
        'pastellviolett' => '#988fa0',
        'gelbgrün' => '#b3ff00',
        'hellgrün' => '#a3b37a',
        'dunkelgrün' => '#518471',
        'blaugrün' => '#318294',
        'türkis' => '#298fa5',
        'gelb' => '#f5d403',
        'orange' => '#dc9018',
        'apricot' => '#cb957d',
        'rot' => '#b61231',
        'dunkelrot' => '#9c2049',
    ],
];
